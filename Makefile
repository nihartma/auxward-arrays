# Note: pybind11 has to be the same version as used to compile awkward
# Might have to set CPLUS_INCLUDE_PATH and (LD_)LIBRARY_PATH to include/library directory in awkward directory or venv

PYTHON_EXT = auxward_ext$(shell python3-config --extension-suffix)
AWKWARD_FLAGS = -lawkward -lawkward-cpu-kernels
#AWKWARD_FLAGS = -lawkward-static -lawkward-cpu-kernels-static

# has to be the same version that was used to compile awkward
# might need to pass these options manually if different pybind11 headers should be used
PYBIND11_INCLUDES = $(shell python3 -m pybind11 --includes)

all: tre $(PYTHON_EXT)

tre: tre.cxx
	c++ -o tre tre.cxx --std=c++17 $(AWKWARD_FLAGS)

$(PYTHON_EXT): auxward_ext.cxx
	c++ auxward_ext.cxx -o auxward_ext`python3-config --extension-suffix` -O3 -Wall -shared -std=c++17 -fPIC -flto -fvisibility=hidden $(AWKWARD_FLAGS) $(PYBIND11_INCLUDES)
