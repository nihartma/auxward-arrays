#include <pybind11/pybind11.h>
#include "edm.h"

// Test for creating a pybind11 extension using awkward

namespace py = pybind11;

std::shared_ptr<ak::ListOffsetArray64> create_test_array() {
  std::vector<std::vector<double>> vv;
  vv.emplace_back(std::vector<double>{1, 2, 3});
  vv.emplace_back(std::vector<double>{1});
  vv.emplace_back(std::vector<double>{});
  vv.emplace_back(std::vector<double>{3, 4});
  return jagged_from_data(vv);
}

PYBIND11_MODULE(auxward_ext, m) {
  py::module::import("awkward1");
  m.doc() = "pybind11 example plugin";
  m.def("create_test_array", &create_test_array, "test");
}
