#include "edm.h"
#include "tools.h"
#include <iostream>
#include <stdio.h>

int main(){

    SomeTool tool;

    std::cout << "hello world" << std::endl;

    FILE* file = fopen("jets.json", "rb");
    std::shared_ptr<ak::Content> out(nullptr);
    auto events = std::dynamic_pointer_cast<ak::RecordArray>(FromJsonFile(file, ak::ArrayBuilderOptions(100, 2.0), 65536));

    for(int event_idx = 0; event_idx < events->length();++event_idx){
        std::cout << "Event: " << event_idx << std::endl;
        std::cout << "=======" << std::endl;
        JetAuxStore jet_aux(events,event_idx,"jets");
        auto jets = JetContainer(jet_aux);
        tool.calibrate(jets);
        for(int i = 0; i < jets.size();++i){
            std::cout << "Jet " << i << "| eta: " << jets.at(i).eta() << " pt: " << jets.at(i).pt() << std::endl;
        }
        std::cout << "=======" << std::endl;
    }
    return 0;
}
