#ifndef EDM
#define EDM

#include "awkward/Identities.h"
#include "awkward/array/RawArray.h"
#include "awkward/array/NumpyArray.h"
#include "awkward/array/ListArray.h"
#include "awkward/array/ListOffsetArray.h"
#include "awkward/array/RecordArray.h"
#include "awkward/array/Record.h"
#include "awkward/Slice.h"

#include "awkward/builder/ArrayBuilder.h"
#include "awkward/builder/ArrayBuilderOptions.h"
namespace ak = awkward;


std::shared_ptr<ak::Content> records_from_jagged(const std::vector<std::shared_ptr<ak::ListOffsetArray64> >& jaggeds, const std::vector<std::string>& names){
std::vector<std::shared_ptr<ak::Content>> recordcontents({ jaggeds[0]->content() , jaggeds[1]->content() });
auto what = std::make_shared<std::vector<std::string>>(names);
std::shared_ptr<ak::util::RecordLookup> recordlookup(what);

auto offsets = jaggeds[0]->offsets();

std::shared_ptr<ak::Content> recordarray = std::make_shared<ak::RecordArray>(ak::Identities::none(), ak::util::Parameters(), recordcontents, recordlookup);
std::shared_ptr<ak::Content> listoffsetarray = std::make_shared<ak::ListOffsetArray64>(ak::Identities::none(), ak::util::Parameters(), offsets, recordarray);
return listoffsetarray;
}

std::shared_ptr<ak::RecordArray> events_from_records(const std::vector<std::shared_ptr<ak::Content> >& collections, const std::vector<std::string>& names){
std::shared_ptr<ak::util::RecordLookup> lookup_event(std::make_shared<std::vector<std::string>>(names));
return std::shared_ptr<ak::RecordArray>(std::make_shared<ak::RecordArray>(
    ak::Identities::none(),
    ak::util::Parameters(),
    collections,
    lookup_event
));
}


double access_helper(const std::shared_ptr<ak::NumpyArray>& a){
return *reinterpret_cast<double*>(reinterpret_cast<ssize_t>(a->ptr().get())+a->byteoffset());
}

double& setting_helper(const std::shared_ptr<ak::NumpyArray>& a){
return *reinterpret_cast<double*>(reinterpret_cast<ssize_t>(a->ptr().get())+a->byteoffset());
}

std::shared_ptr<ak::ListOffsetArray64> jagged_from_data(const std::vector<std::vector<double>>& data){
  ak::ArrayBuilder builder(ak::ArrayBuilderOptions(1024, 2.0));
  for (auto list: data) {
    builder.beginlist();
    for (auto x: list) {
      builder.real(x);
    }
    builder.endlist();
  }
  auto snap = builder.snapshot();
  auto jagged = std::dynamic_pointer_cast<ak::ListOffsetArray64>(snap);
  return jagged;
}

class JetAuxStore{
public:
    JetAuxStore(std::shared_ptr<ak::RecordArray>& events, int event_idx, const std::string& name) : m_events(events), m_eventindex(event_idx), m_name(name){
    }

    double get(int index, const std::string& attr) const {
        return access_helper(
        std::dynamic_pointer_cast<ak::NumpyArray>(
            m_events->getitem_at(m_eventindex)->getitem_field(m_name)->getitem_at(index)->getitem_field(attr)
            )
        );
    }
    void set(int index, const std::string& attr, double value) const {
            setting_helper(
            std::dynamic_pointer_cast<ak::NumpyArray>(
                m_events->getitem_at(m_eventindex)->getitem_field(m_name)->getitem_at(index)->getitem_field(attr)
            ) 
            ) = value;
        }

    int size() const {
        return m_events->getitem_at(m_eventindex)->getitem_field(m_name)->length();
    }

    std::shared_ptr<ak::RecordArray> m_events;
    int m_eventindex;
    std::string m_name;
};

class Jet{
public:
Jet(int idx, const JetAuxStore& aux): m_idx(idx), m_aux(aux){}
    double pt() const {return m_aux.get(m_idx,"pt");}
    double eta() const {return m_aux.get(m_idx,"eta");}
    void set_pt(double value) { return m_aux.set(m_idx,"pt",value); } 
    void set_eta(double value) { return m_aux.set(m_idx,"eta",value); } 

    const JetAuxStore& m_aux;
    int m_idx;
};

class JetContainer{
public:
JetContainer(const JetAuxStore& aux):m_aux(aux){}
    int size(){return m_aux.size();}
    Jet at(int idx){return Jet(idx,m_aux);}
    const JetAuxStore& m_aux;
};

#endif
